package main

import (
	"errors"
	"io/ioutil"
	"sort"
	"strings"

	log "github.com/sirupsen/logrus"
)

func main() {
	bytes, err := ioutil.ReadFile("input")
	handleError("error opening file", err)
	lines := strings.Split(string(bytes), "\n")

	// Temp Variable to hold the highest SeatID
	var highestInTheRoom int = 0

	// Variable for part2
	var allSeatIDs []int

	for k, v := range lines {
		log.Infoln("Running SeatID Calculations for #", k)
		buf := strings.Split(v, "")
		log.Infoln("Our Boarding Pass is", buf)
		resRow, resCol := getRowAndCol(buf)
		result := getSeatID(resRow, resCol)
		allSeatIDs = append(allSeatIDs, result)
		if result > highestInTheRoom {
			highestInTheRoom = result
		}
	}
	log.Warnln("Highest Seat ID:", highestInTheRoom)
	sort.Slice(allSeatIDs, func(i, j int) bool { return allSeatIDs[i] < allSeatIDs[j] })
	for k, v := range allSeatIDs {
		if k == 0 {
			continue
		}
		if allSeatIDs[k+1]-allSeatIDs[k-1] != 2 {
			log.Warnln("Your Seat:", v+1)
			break
		}
	}
}

func getSeatID(row int, col int) (result int) {
	return (row * 8) + col
}

func getRowAndCol(input []string) (rowResult int, colResult int) {
	var minRow, maxRow = 0, 127
	var minCol, maxCol = 0, 7
	var rowResultBuffer, colResultBuffer = 0, 0

	// Initializing buffers
	var rowBuffer [128]int
	for i := 0; i < 128; i++ {
		rowBuffer[i] = i
	}
	var colBuffer [8]int
	for j := 0; j < 8; j++ {
		colBuffer[j] = j
	}
	log.Infoln("Rows:", input[:7])
	log.Infoln("Cols:", input[7:])
	for k, v := range input[:7] {
		switch v {
		case "F":
			if k == len(input[:7])-1 {
				log.Infoln("As our last char is a 'F', we take the lower one:", minRow)
				rowResultBuffer = minRow
			}
			bufferLength := len(rowBuffer[minRow:maxRow]) + 1
			maxRow = maxRow - bufferLength/2
			log.Debugln("New Max Row:", maxRow)

		case "B":
			if k == len(input[:7])-1 {
				log.Infoln("As our last char is a 'B', we take the upper one:", maxRow)
				rowResultBuffer = maxRow
			}
			bufferLength := len(rowBuffer[minRow:maxRow]) + 1
			minRow = minRow + bufferLength/2
			log.Debugln("New Min Row:", minRow)

		}

	}
	for k, v := range input[7:] {
		switch v {
		case "L":
			if k == len(input[7:])-1 {
				log.Infoln("As our last char is a 'L', we take the lower one:", minCol)
				colResultBuffer = minCol
			}
			bufferLength := len(colBuffer[minCol:maxCol]) + 1
			maxCol = maxCol - bufferLength/2
			log.Debugln("New Min Col:", minRow)
		case "R":
			if k == len(input[7:])-1 {
				log.Infoln("As our last char is a 'R', we take the upper one:", maxCol)
				colResultBuffer = maxCol
			}
			bufferLength := len(colBuffer[minCol:maxCol]) + 1
			minCol = minCol + bufferLength/2
			log.Debugln("New Max Col:", maxCol)
		}
	}
	return rowResultBuffer, colResultBuffer

}

func handleError(message string, err error) {
	if err != nil {
		errorText := message + err.Error()
		errbuf := errors.New(errorText)
		log.Fatalln(errbuf)
	}
}
