package main

import (
	"io/ioutil"
	"os"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"
)

var resultPartOne int

func main() {
	file, err := ioutil.ReadFile("input")
	if err != nil {
		log.Fatalln("Could not open file", err)
	}

	lines := strings.Split(string(file), "\n")

	var numbers []int

	for k, v := range lines {
		log.Debugln("Converting line #", k)
		num, err := strconv.Atoi(v)
		if err != nil {
			log.Fatalln(err)
		}
		numbers = append(numbers, num)
	}

	log.Debugln("Finished adding numbers to working array")
	for i := 25; i < len(numbers); i++ {
		// get the last 25
		var last25 [25]int
		var counter int = 0
		// get the last 25 and put them in a slice
		for j := i - 25; j < i; j++ {
			last25[counter] = numbers[j]
			counter++
		}
		// Print out what we got
		log.Debugln("last 25 in round:", i, last25)
		log.Debugln("Sum should be:", numbers[i])

		// go through all the possible combinations and see if there's a match
		// temp var to store if the sum is there or not
		var sumIsThere bool = false
		for x := range last25 {
			for y := range last25 {
				if last25[x]+last25[y] == numbers[i] {
					log.Debugln("This number is valid. Continuing.", numbers[i])
					sumIsThere = true
				}
				// If we got a correct sum, try the next one
				if sumIsThere {
					break
				}
			}
			if sumIsThere {
				break
			}
		}
		if !sumIsThere {
			log.Warnln("PART ONE:")
			log.Warnln("Number", numbers[i], "does not have a sum in the previous 25.")
			resultPartOne = numbers[i]
		}
	}
	// Part Two

	for x := range numbers {
		var buffer int = 0
		log.Debugln("Round", x)

		var numbersCalc []int
		for counter := x; counter < len(numbers); counter++ {
			if buffer < resultPartOne {
				log.Debugln("Buffer:", buffer, "Target:", resultPartOne)
				buffer = buffer + numbers[counter]
				numbersCalc = append(numbersCalc, numbers[counter])
			}
			if buffer == resultPartOne {
				log.Warnln("We got a result for part two:", numbersCalc)
				min, max := MinMax(numbersCalc)
				log.Warnln("Min and Max of the contiguous set:", min, max)
				log.Warnln("Result of Part two:", min+max)
				os.Exit(0)
			}
			if buffer > resultPartOne {
				log.Debugln("Too big, next one..")
				break
			}
		}
	}

}

// MinMax returns the minimum and maximum of an array / slice
func MinMax(array []int) (int, int) {
	var max int = array[0]
	var min int = array[0]
	for _, value := range array {
		if max < value {
			max = value
		}
		if min > value {
			min = value
		}
	}
	return min, max
}
