package main

import (
	"io/ioutil"
	"os"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"
)

func main() {
	file, err := os.Open("input")
	if err != nil {
		log.Println(err)
	}
	bytes, err := ioutil.ReadAll(file)
	if err != nil {
		log.Println(err)
	}

	// Converting Strings to integers
	buf := strings.Split(string(bytes), "\n")
	var inputSlice = make([]int, len(buf))

	for k, v := range buf {
		inputSlice[k], err = strconv.Atoi(v)
		if err != nil {
			log.Println(err)
		}
	}

	var bufRes, endResult int
	for k, v := range inputSlice {
		for j, x := range inputSlice {
			if k == len(inputSlice)-1 {
				break
			}
			bufRes = v + x
			if bufRes == 2020 {
				log.Warnf("Value %d on %d and value %d on %d are 2020 when we are summarizing them.", v, k, x, j)
				endResult = v * x
				log.Fatalln("End Result is:", endResult)
			}
		}
	}

}
