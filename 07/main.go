package main

import (
	"io/ioutil"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"
)

type bag struct {
	count int
	color string
}

func main() {

	file, err := ioutil.ReadFile("input")
	if err != nil {
		log.Fatalln("Could not read file", err)
	}
	lines := strings.Split(string(file), "\n")
	bagRules := make(map[string][]bag)
	for _, v := range lines {
		var validLine bool = true
		// log.Println(v)
		s := strings.Split(v, " ")

		// Check how many commas are there and if there is a "no"
		var commaCounter int = 0
		for x, y := range s {
			if strings.Contains(y, ",") {
				log.Println("Contains one comma at", x)
				commaCounter++
			}

			if strings.Contains(y, "no") {
				log.Println("Can ignore this bag. Contains no other bags.")
				validLine = false
			}
		}
		if !validLine {
			continue
		}

		// Get the first bag
		firstBag := s[0] + " " + s[1]
		log.Println("First Bag:", firstBag)

		// lets get the other bags of the string
		for i := 1; i <= commaCounter+1; i++ {

			firstIndex := 1 + 4*i
			countIndex := firstIndex - 1
			secondIndex := firstIndex + 2
			// log.Println(i+1, "Bag:", strings.Join(s[firstIndex:secondIndex], " "), "Count:", s[countIndex:firstIndex])

			// Converting count in int
			countString := strings.Join(s[countIndex:firstIndex], "")
			countInt, err := strconv.Atoi(countString)
			if err != nil {
				log.Fatalln(err)
			}
			bagRules[firstBag] = append(bagRules[firstBag], bag{count: countInt, color: strings.Join(s[firstIndex:secondIndex], " ")})
		}

	}
	log.Println(bagRules)
	shinyGoldCounter := 0
	// var otherColors []string
	log.Println(getBagWhichContainsColor(bagRules, "shiny gold"))

	log.Println("There are", shinyGoldCounter, "bags which can contain a shiny gold bag _directly_.")
	log.Println(shinyGoldCounter)
	// Part 1
	contains := make(map[string]struct{})
	check("shiny gold", bagRules, contains)
	log.Println(len(contains))
}

func check(color string, rules map[string][]bag, contains map[string]struct{}) {
	for k := range getBagWhichContainsColor(rules, color) {
		contains[k] = struct{}{}
		check(k, rules, contains)
	}
}

func getBagWhichContainsColor(bagRules map[string][]bag, color string) map[string]struct{} {
	otherColorBuffer := make(map[string]struct{})
	for fb, rule := range bagRules {
		// Going through each of those bags
		for _, v := range rule {
			if v.color == color {
				log.Println("we got a match, outer bag is:", fb)
				otherColorBuffer[fb] = struct{}{}
			}
		}
	}
	return otherColorBuffer
}
