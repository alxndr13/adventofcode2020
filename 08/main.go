package main

import (
	"io/ioutil"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"
)

type instruction struct {
	instr string
	value int
}

func main() {
	file, err := ioutil.ReadFile("input")
	if err != nil {
		log.Fatalln(err)
	}
	lines := strings.Split(string(file), "\n")

	// Create a map holding all the necessarv information
	bootLoader := make(map[int]instruction)

	for k, v := range lines {
		instr := strings.Split(v, " ")[0]
		value := strings.Split(v, " ")[1]
		valueInt, err := strconv.Atoi(value)
		if err != nil {
			log.Fatalln(err)
		}
		i := instruction{
			instr: instr,
			value: valueInt,
		}
		bootLoader[k] = i
	}
	log.Debugln("Result:")
	log.Debugln(bootLoader)
	log.Println("PART ONE: Accumulator Result:", runCheck(bootLoader))
	log.Println("PART TWO:")
	fixCode(bootLoader)

}

func fixCode(bootLoaderInput map[int]instruction) {
	var opsIndices []int
	// get all the indices of "jmp"'s
	for k := range bootLoaderInput {
		if bootLoaderInput[k].instr == "nop" {
			opsIndices = append(opsIndices, k)
		}
	}
	// Switch out each jmp with a nop and run the program again
	for x := range opsIndices {
		var bootLoaderCopy = make(map[int]instruction)
		for k, v := range bootLoaderInput {
			if k == x && k != 0 {
				newV := instruction{
					value: v.value,
					instr: "jmp",
				}
				bootLoaderCopy[k] = newV
				// log.Println("New one:", bootLoaderCopy[k])
				// log.Println(bootLoaderCopy)
				// log.Println("Running with a changed jmp on index", x, "- Result:", runCheck(bootLoaderCopy))
				log.Println("Run #", x)
				log.Println(runCheck(bootLoaderCopy))
			} else {
				bootLoaderCopy[k] = v
			}
		}

	}
}

// Part 1
func runCheck(bootLoaderInput map[int]instruction) (acc int) {
	// holding all the instructions we already used
	var alreadyUsedInstr []int
	// the global value which we need
	var accumulator int = 0
	var stillValid bool = true
	for i := 0; i < len(bootLoaderInput); i++ {
		log.Debugln("i is:", i)
		val := bootLoaderInput[i].value
		instr := bootLoaderInput[i].instr

		log.Debugln("Instruction:", instr)
		log.Debugln("Value:", val)
		log.Debugln("Accumulator:", accumulator)

		switch instr {
		case "acc":
			if contains(alreadyUsedInstr, i) {
				log.Warnln("We got a loop here, accumulator is:", accumulator)
				stillValid = false
				break
			}
			accumulator = accumulator + val
			alreadyUsedInstr = append(alreadyUsedInstr, i)
		case "nop":
			alreadyUsedInstr = append(alreadyUsedInstr, i)
			if i == len(bootLoaderInput)-2 && bootLoaderInput[i+1].instr == "acc" {
				log.Println("second last instruction nop")
			}
		case "jmp":
			alreadyUsedInstr = append(alreadyUsedInstr, i)
			if i == len(bootLoaderInput)-2 && (bootLoaderInput[i+1].instr == "acc" || bootLoaderInput[i].instr == "nop") {
				log.Println("second last instruction jmp")
				// accumulator = accumulator + bootLoaderInput[i+1].value
				// log.Fatalln("Program terminates with acc:", accumulator)
			}
			if val == 1 {
				log.Debugln("This is a jmp with", val)
				continue
			}
			log.Debugln("This is a jmp with", val)
			i = i + val - 1
		}
		log.Debugln(alreadyUsedInstr)
		if !stillValid {
			break
		}
	}

	return accumulator
}

func contains(s []int, e int) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}
