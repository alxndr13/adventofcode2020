package main

import (
	"errors"
	"io/ioutil"
	"strings"

	"github.com/mpvl/unique"
	log "github.com/sirupsen/logrus"
)

func main() {
	bytes, err := ioutil.ReadFile("input")
	handleError("error opening file", err)
	groups := strings.Split(string(bytes), "\n\n")

	var result int = 0

	for _, v := range groups {
		var answerSlice []string
		personsAnswers := strings.Split(v, "\n")
		for _, x := range personsAnswers {
			answers := strings.Split(x, "")
			answerSlice = append(answerSlice, answers...)
		}
		var s unique.StringSlice
		s.P = &answerSlice
		unique.Sort(s)
		log.Println(len(*s.P))
		result = result + len(*s.P)
	}
	log.Warnln("Part One:", result)

	var globalYesCounter int
	for k, v := range groups {
		var answersPerGroup []string
		answerPerPerson := strings.Split(v, "\n")
		personCount := len(answerPerPerson)
		log.Infoln("There are", personCount, "persons in group #", k)
		for _, x := range answerPerPerson {
			answers := strings.Split(x, "")
			answersPerGroup = append(answersPerGroup, answers...)
		}
		log.Infoln("the group", k, "answered 'yes' to the following questions:", answersPerGroup)
		copyAnswersPerGroup := answersPerGroup
		var uniqueAnswers unique.StringSlice
		uniqueAnswers.P = &answersPerGroup
		unique.Sort(uniqueAnswers)
		log.Infoln("Unique Answers:", *uniqueAnswers.P)
		var uniqueAnswersCounter = make(map[string]int)
		for _, v := range *uniqueAnswers.P {
			for _, y := range copyAnswersPerGroup {
				if v == y {
					// log.Infoln("Compare '", v, "' to '", y, "'")
					log.Infoln("Unique Answer", v, "found.")
					uniqueAnswersCounter[v] = uniqueAnswersCounter[v] + 1
					log.Infoln("Counter:", uniqueAnswersCounter[v])
				}
			}
		}
		buf := 0
		for k := range uniqueAnswersCounter {

			// log.Infoln(uniqueAnswersCounter)
			if uniqueAnswersCounter[k] == personCount {
				log.Println("Everyone answered to the question", k, "with yes.")
				buf = buf + 1
			}
		}
		globalYesCounter = globalYesCounter + buf

	}
	log.Warnln("Sum of counts where everyone answered yes:", globalYesCounter)
}

func handleError(message string, err error) {
	if err != nil {
		errorText := message + err.Error()
		errbuf := errors.New(errorText)
		log.Fatalln(errbuf)
	}
}
