package main

import (
	"io/ioutil"
	"os"
	"regexp"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"
)

// byr (Birth Year)
// iyr (Issue Year)
// eyr (Expiration Year)
// hgt (Height)
// hcl (Hair Color)
// ecl (Eye Color)
// pid (Passport ID)
// cid (Country ID)

type passport struct {
	byr int
	iyr int
	eyr int
	hcl string
	ecl string
	pid string
	cid string
	hgt string
}

func main() {

	file, err := os.Open("input")
	if err != nil {
		log.Println(err)
	}

	bytes, err := ioutil.ReadAll(file)
	if err != nil {
		log.Println(err)
	}

	// Variable, where we store all the parsed passports
	var passports []passport

	// Split at the empty lines
	bufLines := strings.Split(string(bytes), "\n\n")

	// Go through each passport entry
	for _, v := range bufLines {
		// Remove newlines in a single passport entry
		newLine := strings.Replace(v, "\n", " ", -1)
		// Generate buffer variable to hold our current passport
		var p passport

		// put each field in a slice
		bufArr := strings.Split(newLine, " ")
		// iterate over each field and see which of the fields it is
		for _, y := range bufArr {
			bb := strings.Split(y, ":")

			// This should be self-explanatory
			switch bb[0] {
			case "byr":
				p.byr, _ = strconv.Atoi(bb[1])
			case "iyr":
				p.iyr, _ = strconv.Atoi(bb[1])
			case "eyr":
				p.eyr, _ = strconv.Atoi(bb[1])
			case "hcl":
				p.hcl = bb[1]
			case "ecl":
				p.ecl = bb[1]
			case "pid":
				p.pid = bb[1]
			case "cid":
				p.cid = bb[1]
			case "hgt":
				p.hgt = bb[1]
			default:
				log.Warnln("invalid or missing field", bb[1])
			}
		}
		// See what we did over there
		log.Debugln(p)

		// Append to our global passports slice
		passports = append(passports, p)
	}
	log.Println("We parsed all", len(passports), "passports successfully. :)")

	// Now let's check which passwords are valid

	// Holds our count of valid passports
	var validCounter int

	for k, v := range passports {
		log.Infoln("Working on passport #", k+1)
		if v.cid == "" {
			log.Debugln("CID is empty, skipping.")
		}

		// Validating Birth Year
		if v.byr == 0 || v.byr < 1920 || v.byr > 2002 {
			log.Warnln("Birthyear", v.byr, "is invalid")
			continue
		}
		// Validating Issue Year
		if v.iyr == 0 || v.iyr < 2010 || v.iyr > 2020 {
			log.Warnln("Issueyear", v.iyr, "is invalid")
			continue
		}
		// Validating Expire Year
		if v.eyr == 0 || v.eyr < 2020 || v.eyr > 2030 {
			log.Warnln("Expireyear", v.eyr, "is invalid")
			continue
		}

		// Validate Eye Color
		eyeColorValid := true
		if v.ecl == "" {
			log.Warnln("Eye Color is empty")
			continue
		}
		switch v.ecl {
		case "amb", "blu", "brn", "gry", "grn", "hzl", "oth":
			log.Debugln("Eyecolor is valid")
		default:
			log.Warnln("Eyecolor", v.ecl, "is not a valid color")
			eyeColorValid = false
		}
		if !eyeColorValid {
			continue
		}

		// Validate Hair Color
		if v.hcl == "" {
			log.Warnln("Hair Color is empty")
			continue
		}
		matched, err := regexp.MatchString(`^#[0-9a-f]{6}$`, v.hcl)
		if err != nil {
			log.Fatalln("Could not match string of Hair Color with regexp", err)
		}
		// If the hair color does not match our regexp, it is not a valid color and therefore not a valid passport
		if !matched {
			log.Warnln("Hair Color", v.hcl, "is invalid")
			continue
		}

		// Validate Height
		if len(v.hgt) == 0 {
			log.Warnln("Height is empty")
			continue
		}
		matchedHeight, err := regexp.MatchString(`^([0-9]{1,3})(cm|in)$`, v.hgt)
		if err != nil {
			log.Fatalln("Could not match string of Height with regexp", err)
		}
		if !matchedHeight {
			log.Warnln("Height", v.hgt, "is invalid")
			continue
		}
		// inches
		if strings.HasSuffix(v.hgt, "in") {
			numberBuf := strings.Trim(v.hgt, "in")
			actualNumber, err := strconv.Atoi(numberBuf)
			if err != nil {
				log.Fatalln("Unable to parse height in inches", err)
			}
			if actualNumber < 59 || actualNumber > 76 {
				log.Warnln("Height in inches is invalid, got", v.hgt)
				continue
			}
		}
		// cm
		if strings.HasSuffix(v.hgt, "cm") {
			numberBuf := strings.Trim(v.hgt, "cm")
			actualNumber, err := strconv.Atoi(numberBuf)
			if err != nil {
				log.Fatalln("Unable to parse height in cm", err)
			}
			if actualNumber < 150 || actualNumber > 193 {
				log.Warnln("Height in cm is invalid, got", v.hgt)
				continue
			}
		}
		// Validate Passport ID
		if v.pid == "" {
			log.Warnln("PassportID is empty")
			continue
		}
		matchedPID, err := regexp.MatchString(`^[0-9]{9}$`, v.pid)
		if err != nil {
			log.Fatalln("Could not match string of PassportID with regexp", err)
		}
		if !matchedPID {
			log.Warnln("PassportID", v.pid, "is invalid")
			continue
		}
		validCounter++
		log.Infoln("Passport #", k+1, "is valid. Current count of valid passports:", validCounter)
	}
	log.Fatalln("Valid Passports:", validCounter)
}
