package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"
)

type passwordPolicy struct {
	minOccurences int
	maxOccurences int
	letterOf      string
	password      string
}
type passwordPolicies struct {
	policies []passwordPolicy
}

func main() {
	file, err := os.Open("input")
	if err != nil {
		log.Println(err)
	}
	bytes, err := ioutil.ReadAll(file)
	if err != nil {
		log.Println(err)
	}

	// Converting to single lines
	bufLines := strings.Split(string(bytes), "\n")

	// Buffer to hold our results
	var pRes passwordPolicies

	for k, v := range bufLines {
		if k == len(bufLines)-1 {
			log.Warnln("The end.")
			break
		}
		var p passwordPolicy
		indexN1 := strings.IndexByte(v, '-')
		n1 := v[:indexN1]
		indexN2 := strings.IndexByte(v, ' ')
		n2 := v[indexN1+1 : indexN2]
		indexBuchstabe1 := strings.IndexByte(v, ' ')
		indexBuchstabe2 := strings.IndexByte(v, ':')
		buchstabe := v[indexBuchstabe1+1 : indexBuchstabe2]
		password := v[indexBuchstabe2+2:]
		log.Debugln("Line:", k, "has number1:", n1, "and number2:", n2, "with buchstabe:", buchstabe, "with password:", password)
		p.minOccurences, err = strconv.Atoi(n1)
		if err != nil {
			log.Errorln("Could not parse int")
		}
		p.maxOccurences, err = strconv.Atoi(n2)
		if err != nil {
			log.Errorln("Could not parse int")
		}
		p.letterOf = buchstabe
		p.password = password
		pRes.policies = append(pRes.policies, p)
	}
	log.Println("Resulting Struct:")
	log.Println(pRes)
	var validCounter int = 0
	for _, v := range pRes.policies {
		count := strings.Count(v.password, v.letterOf)
		if count > v.maxOccurences {
			log.Warnln("Not valid")
			continue
		} else if count < v.minOccurences {
			log.Warnln("Not valid")
			continue
		}
		validCounter++
	}
	fmt.Println("Valid Passwords:", validCounter)

}
