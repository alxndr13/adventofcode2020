package main

import (
	"io/ioutil"
	"os"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"
)

func main() {
	file, err := os.Open("input")
	if err != nil {
		log.Println(err)
	}

	bytes, err := ioutil.ReadAll(file)
	if err != nil {
		log.Println(err)
	}

	bufLines := strings.Split(string(bytes), "\n")
	log.Infoln("We have", strconv.Itoa(len(bufLines)), "Lines")
	log.Infoln("To make it to the bottom with 3 steps to the side we need", strconv.Itoa(3*len(bufLines)), "columns")
	log.Infoln("Currently we have", strconv.Itoa(len(bufLines[0])), "columns")
	log.Infoln("Therefore we need", strconv.Itoa(3*len(bufLines)-len(bufLines[0])), "more columns")
	log.Infoln("We need to repeat the biome", strconv.Itoa(3*len(bufLines)/len(bufLines[0])), "times")
	inputMap := make([][]string, len(bufLines)-1)
	for k, v := range bufLines {
		if v == "" {
			break
		}
		lineSplit := strings.Split(v, "")
		inputMap[k] = repeatedSlice(lineSplit, 32)
		log.Infoln("Line", k, "is now", len(inputMap[k]), "columns long")
	}
	// Print Map
	// for k, v := range inputMap {
	// 	log.Println(k, ": ", v)
	// }

	var sideStep, verticalStep int = 0, 0
	var treesFound int = 0
	for k := range inputMap {
		if inputMap[verticalStep][sideStep] == "#" {
			log.Infoln("We found a tree at line", k)
			treesFound++
		}
		sideStep = sideStep + 3
		log.Infoln("Sidestep:", sideStep)
		verticalStep = verticalStep + 1
		log.Infoln("VerticalStep aka Line", verticalStep)
	}
	log.Fatalln("We found", treesFound, "trees.")

}

func repeatedSlice(value []string, n int) []string {
	var arr []string
	for i := 0; i <= n; i++ {
		arr = append(arr, value...)
	}
	return arr
}
