package main

import (
	"testing"

	log "github.com/sirupsen/logrus"
)

type testCaseDecoding struct {
	input  []string
	result []int
}

type testCaseSeatID struct {
	input  []int
	result int
}

func TestGetRowAndCol(t *testing.T) {
	var testcases []testCaseDecoding
	r1 := testCaseDecoding{
		input:  []string{"F", "B", "F", "B", "B", "F", "F", "R", "L", "R"},
		result: []int{44, 5},
	}
	testcases = append(testcases, r1)

	r2 := testCaseDecoding{
		input:  []string{"F", "F", "F", "B", "B", "B", "F", "R", "L", "R"},
		result: []int{14, 5},
	}
	testcases = append(testcases, r2)

	for k, v := range testcases {
		log.Infoln("Test Case #", k)
		resRow, resCol := getRowAndCol(v.input)
		if resRow != v.result[0] || resCol != v.result[1] {
			t.Errorf("Expected %d, got %d and %d", v.result, resRow, resCol)
		}
	}
}

func TestSeatIDCalc(t *testing.T) {
	t1 := testCaseSeatID{
		input:  []int{70, 7},
		result: 567,
	}
	t2 := testCaseSeatID{
		input:  []int{14, 7},
		result: 119,
	}
	t3 := testCaseSeatID{
		input:  []int{102, 4},
		result: 820,
	}
	var testcases []testCaseSeatID
	testcases = append(testcases, t1)
	testcases = append(testcases, t2)
	testcases = append(testcases, t3)
	for k, v := range testcases {
		log.Infoln("Test Case #", k)
		res := getSeatID(v.input[0], v.input[1])
		if res != v.result {
			t.Errorf("Expected %d, got %d", v.result, res)
		}
	}
}
